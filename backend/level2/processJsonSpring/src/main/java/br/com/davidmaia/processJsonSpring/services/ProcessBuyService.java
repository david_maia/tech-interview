package br.com.davidmaia.processJsonSpring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Cart;
import br.com.davidmaia.processJsonSpring.models.DeliveryFees;
import br.com.davidmaia.processJsonSpring.models.Item;

/**
 * 
 * @author davidkmaia
 *
 */

@Service
public class ProcessBuyService {

	@Autowired
	private ArticleService articleService;

	@Autowired
	private DeliveryFeesService deliveryFeesService;

	/**
	 * Process the carts buy
	 * @param carts
	 * @param articlesList
	 * @param deliveryFeesList
	 * @return list of carts updated with prices
	 */
	public List<Cart> processBuyCarts(List<Cart> carts, List<Article> articlesList,
			List<DeliveryFees> deliveryFeesList) {
		calculePriceTotalCart(carts, articlesList, deliveryFeesList);
		return carts;
	}

	/**
	 * Popule the carts with total price and article
	 * @param carts
	 * @param articlesList
	 * @param deliveryFeesList
	 */
	private void calculePriceTotalCart(List<Cart> carts, List<Article> articlesList,
			List<DeliveryFees> deliveryFeesList) {
		for (Cart cart : carts) {
			if (cart.getItems() != null) {
				for (Item item : cart.getItems()) {
					item.setArticle(articleService.findById(item.getArticleId(), articlesList));
					if (item.getArticle() != null) {
						cart.setTotal(cart.getTotal() + (item.getArticle().getPrice() * item.getQuantity()));
					}
				}
			}
			cart.setTotal(
					cart.getTotal() + deliveryFeesService.calculePriceDelivery(cart.getTotal(), deliveryFeesList));
		}
	}

	public void setArticleService(ArticleService articleService) {
		this.articleService = articleService;
	}
	
	public void setDeliveryFeesService(DeliveryFeesService deliveryFeesService){
		this.deliveryFeesService = deliveryFeesService;
	}

}
