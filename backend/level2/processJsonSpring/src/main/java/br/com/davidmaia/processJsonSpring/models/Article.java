package br.com.davidmaia.processJsonSpring.models;

/**
 * 
 * @author davidkmaia
 *
 */
public class Article extends AbstractModel<Long> {

	private String name;
	private Long price;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Article() {

	}

	public Article(String name, Long price) {
		super();
		this.name = name;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Article [name=" + name + ", price=" + price + "]";
	}

}
