package br.com.davidmaia.processJsonSpring.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ArrayNode;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Cart;
import br.com.davidmaia.processJsonSpring.models.DeliveryFees;
import br.com.davidmaia.processJsonSpring.models.Views;
import br.com.davidmaia.processJsonSpring.services.ProcessBuyService;

/**
 * 
 * @author davidkmaia
 *
 */
@RestController
public class ProcessBuyController {
	
	@Autowired
	private ProcessBuyService processBuyService;
	
	@Autowired
    ObjectMapper mapper;

	/**
	 * Method responsible for process the Json request
	 * @param str
	 * @return
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/process", method = RequestMethod.POST)
	public String processJson(@RequestBody String str) throws JsonProcessingException, IOException {
		List<Article> articlesList = new ArrayList<Article>();
		List<Cart> carts = new ArrayList<Cart>();
		List<DeliveryFees> deliveryFeesList = new ArrayList<DeliveryFees>();
		
		readJsonRequest(str, articlesList, carts, deliveryFeesList);
		
		carts = processBuyService.processBuyCarts(carts, articlesList, deliveryFeesList);
		
		ObjectWriter viewWriter = mapper.writerWithView(Views.Public.class);
		return viewWriter.withRootName("carts").writeValueAsString(carts);
	}

	/**
	 * Makes read of json
	 * @param str
	 * @param articlesList
	 * @param carts
	 * @param deliveryFeesList
	 * @throws IOException
	 * @throws JsonProcessingException
	 */
	private void readJsonRequest(String str, List<Article> articlesList, List<Cart> carts, 
			List<DeliveryFees> deliveryFeesList) throws IOException, JsonProcessingException {
		mapper = new ObjectMapper();
		JsonNode nodeMapper = mapper.readTree(str);
		
		populeArticles(articlesList, nodeMapper);
		populeCarts(carts, nodeMapper);
		populeDeliveryFees(deliveryFeesList, nodeMapper);
	}

	/**
	 * Reads the articles of json
	 * @param articlesList
	 * @param nodeMapper
	 */
	private void populeArticles(List<Article> articlesList, JsonNode nodeMapper) {
		ArrayNode articlesNode = (ArrayNode) nodeMapper.get("articles");
		if(articlesNode != null){
			Iterator<JsonNode> nodeIt = articlesNode.elements();
			while(nodeIt.hasNext()){
				JsonNode node = nodeIt.next();
				Article article = mapper.convertValue(node, Article.class);
				articlesList.add(article);
			}
		}
	}
	
	/**
	 * Reads the carts of json
	 * @param carts
	 * @param nodeMapper
	 */
	private void populeCarts(List<Cart> carts, JsonNode nodeMapper) {
		ArrayNode cartsNode = (ArrayNode) nodeMapper.get("carts");
		if(cartsNode != null){
			Iterator<JsonNode> nodeIt = cartsNode.elements();
			while(nodeIt.hasNext()){
				JsonNode node = nodeIt.next();
				Cart cart = mapper.convertValue(node, Cart.class);
				carts.add(cart);
			}
		}
	}
	
	/**
	 * Reads the Delivery fees of json
	 * @param deliveryFeesList
	 * @param nodeMapper
	 */
	private void populeDeliveryFees(List<DeliveryFees> deliveryFeesList, JsonNode nodeMapper) {
		ArrayNode deliveryNode = (ArrayNode) nodeMapper.get("delivery_fees");
		if(deliveryNode != null){
			Iterator<JsonNode> nodeIt = deliveryNode.elements();
			while(nodeIt.hasNext()){
				JsonNode node = nodeIt.next();
				DeliveryFees delivery = mapper.convertValue(node, DeliveryFees.class);
				deliveryFeesList.add(delivery);
			}
		}
	}
	
	
}
