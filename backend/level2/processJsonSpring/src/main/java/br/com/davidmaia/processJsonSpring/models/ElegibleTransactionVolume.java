package br.com.davidmaia.processJsonSpring.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author davidkmaia
 *
 */
public class ElegibleTransactionVolume {
	private Long minPrice;
	private Long maxPrice;

	@JsonProperty("min_price")
	public Long getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Long minPrice) {
		this.minPrice = minPrice;
	}

	@JsonProperty("max_price")
	public Long getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Long maxPrice) {
		this.maxPrice = maxPrice;
	}

	public ElegibleTransactionVolume() {

	}

	public ElegibleTransactionVolume(Long minPrice, Long maxPrice) {
		super();
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((maxPrice == null) ? 0 : maxPrice.hashCode());
		result = prime * result + ((minPrice == null) ? 0 : minPrice.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ElegibleTransactionVolume other = (ElegibleTransactionVolume) obj;
		if (maxPrice == null) {
			if (other.maxPrice != null)
				return false;
		} else if (!maxPrice.equals(other.maxPrice))
			return false;
		if (minPrice == null) {
			if (other.minPrice != null)
				return false;
		} else if (!minPrice.equals(other.minPrice))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ElegibleTransactionVolume [minPrice=" + minPrice + ", maxPrice=" + maxPrice + "]";
	}

}