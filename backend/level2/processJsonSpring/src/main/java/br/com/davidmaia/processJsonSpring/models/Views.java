package br.com.davidmaia.processJsonSpring.models;

/**
 * 
 * @author davidkmaia
 *
 */
public class Views {

	public static interface Public {
	}

	static interface Internal extends Public {
	}
}