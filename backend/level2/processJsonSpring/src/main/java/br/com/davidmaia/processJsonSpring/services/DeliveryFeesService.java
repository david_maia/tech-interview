package br.com.davidmaia.processJsonSpring.services;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.davidmaia.processJsonSpring.models.DeliveryFees;

/**
 * 
 * @author davidkmaia
 *
 */

@Service
public class DeliveryFeesService {

	/**
	 * Returns delivery price for delivery fees list
	 * @param cartValue
	 * @param deliveryFeesList
	 * @return
	 */
	public Long calculePriceDelivery(Long cartValue, List<DeliveryFees> deliveryFeesList) {
		if(deliveryFeesList != null && cartValue != null){
			for(DeliveryFees deliveryFees : deliveryFeesList){
				if(cartValue >= deliveryFees.getElegibleTransactionVolume().getMinPrice() 
						&& deliveryFees.getElegibleTransactionVolume().getMaxPrice() != null 
						&& cartValue < deliveryFees.getElegibleTransactionVolume().getMaxPrice()){
					return deliveryFees.getPrice();
				}
			}
		}
		return 0L;
	}

}
