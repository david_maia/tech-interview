package processJsonSpring;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Cart;
import br.com.davidmaia.processJsonSpring.models.DeliveryFees;
import br.com.davidmaia.processJsonSpring.models.ElegibleTransactionVolume;
import br.com.davidmaia.processJsonSpring.models.Item;
import br.com.davidmaia.processJsonSpring.services.ArticleService;
import br.com.davidmaia.processJsonSpring.services.DeliveryFeesService;
import br.com.davidmaia.processJsonSpring.services.ProcessBuyService;

/**
 * 
 * @author davidkmaia
 *
 */
public class ProcessBuyServiceTest {

	private ProcessBuyService processBuyService = new ProcessBuyService();

	@Before
	public void init() {
		processBuyService.setArticleService(new ArticleService());
		processBuyService.setDeliveryFeesService(new DeliveryFeesService());
	}

	@Test
	public void shouldReturnCartsPopuled() {
		List<Cart> carts = getCarts();

		carts = processBuyService.processBuyCarts(carts, getArticles(), getDeliveryFees());

		Assert.assertTrue(carts.get(0).getItems().get(0).getArticle().getId() == 1l);
		Assert.assertTrue(1400L == carts.get(0).getTotal());
		Assert.assertTrue(2100L == carts.get(1).getTotal());
		Assert.assertTrue(800L == carts.get(2).getTotal());
	}
	
	@Test
	public void shouldReturnCartsWithoutArticles() {
		List<Cart> carts = new ArrayList<Cart>();
		Cart cart1 = new Cart();
		cart1.setId(1l);
		Cart cart2 = new Cart();
		cart2.setId(2l);
		carts.add(cart1);
		carts.add(cart2);

		carts = processBuyService.processBuyCarts(carts, getArticles(), getDeliveryFees());

		Assert.assertNull(carts.get(0).getItems());
		Assert.assertTrue(800L == carts.get(0).getTotal());
		Assert.assertTrue(800L == carts.get(1).getTotal());
	}

	private List<Cart> getCarts() {
		List<Cart> carts = new ArrayList<Cart>();

		Cart cart1 = new Cart();
		cart1.setId(1l);
		cart1.setItems(getItens(1l, 3l));

		Cart cart2 = new Cart();
		cart2.setId(2l);
		cart2.setItems(getItens(1l, 2l, 4l));

		Cart cart3 = new Cart();
		cart3.setId(3l);
		cart3.setItems(new ArrayList<Item>());

		carts.add(cart1);
		carts.add(cart2);
		carts.add(cart3);

		return carts;
	}

	private List<Article> getArticles() {
		List<Article> list = new ArrayList<Article>();
		for (long i = 1; i <= 4; i++) {
			Article a = new Article();
			a.setId(i);
			a.setName("Article " + i);
			a.setPrice(100 * i);
			list.add(a);
		}
		return list;
	}

	private List<Item> getItens(Long... articleId) {
		List<Item> items = new ArrayList<Item>();
		for (Long i : articleId) {
			Item item = new Item();
			item.setArticleId(i);
			item.setQuantity(i.intValue());
			items.add(item);
		}
		return items;
	}
	
	private List<DeliveryFees> getDeliveryFees(){
		List<DeliveryFees> list = new ArrayList<DeliveryFees>();
		DeliveryFees d1 = new DeliveryFees(800L, new ElegibleTransactionVolume(0L, 1000L));
		DeliveryFees d2 = new DeliveryFees(400L, new ElegibleTransactionVolume(1000L, 2000L));
		DeliveryFees d3 = new DeliveryFees(0L, new ElegibleTransactionVolume(2000L, null));
		list.add(d1);
		list.add(d2);
		list.add(d3);
		return list;
	}

}
