# processJsonSpring
Process JSON file with Springboot

###Considerações:
- Criei uma API REST usando SpringBoot. A url http://localhost:8080/processFile Method:POST tanto pode receber um arquivo json(data.json) via binary, quanto o próprio json no body da request. Além de retornar um json na response, o método grava um arquivo output.json na raiz da aplicação.
- Usei a biblioteca jackson para a leitura do json e o swagger para documentação. A documentação via swagger pode ser acessa via browser na url: http://localhost:8080/swagger-ui.html.
- Para testes usei o JUnit.
