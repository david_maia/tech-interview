package br.com.davidmaia.processJsonSpring.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author davidkmaia
 *
 */
@ApiModel(value = "Article")
public class Article extends AbstractModel<Long> {

	@ApiModelProperty(value = "Name of article")
	private String name;
	@ApiModelProperty(value = "Price of article")
	private Long price;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Article() {

	}

	public Article(String name, Long price) {
		super();
		this.name = name;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Article [name=" + name + ", price=" + price + "]";
	}

}
