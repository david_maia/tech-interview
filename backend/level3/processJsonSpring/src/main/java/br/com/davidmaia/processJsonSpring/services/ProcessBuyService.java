package br.com.davidmaia.processJsonSpring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Cart;
import br.com.davidmaia.processJsonSpring.models.DeliveryFees;
import br.com.davidmaia.processJsonSpring.models.Discount;
import br.com.davidmaia.processJsonSpring.models.Item;

/**
 * 
 * @author davidkmaia
 *
 */

@Service
public class ProcessBuyService {

	@Autowired
	private ArticleService articleService;

	@Autowired
	private DeliveryFeesService deliveryFeesService;

	@Autowired
	private DiscountService discountService;

	/**
	 * Process the carts buy
	 * 
	 * @param carts
	 * @param articlesList
	 * @param deliveryFeesList
	 * @param discounts
	 * @return list of carts updated with prices
	 */
	public List<Cart> processBuyCarts(List<Cart> carts, List<Article> articlesList, List<DeliveryFees> deliveryFeesList,
			List<Discount> discounts) {
		calculePriceTotalCart(carts, articlesList, deliveryFeesList, discounts);
		return carts;
	}

	/**
	 * Popule the carts with total price and article
	 * 
	 * @param carts
	 * @param articlesList
	 * @param deliveryFeesList
	 * @param discounts
	 */
	private void calculePriceTotalCart(List<Cart> carts, List<Article> articlesList,
			List<DeliveryFees> deliveryFeesList, List<Discount> discounts) {
		for (Cart cart : carts) {
			if (cart.getItems() != null) {
				for (Item item : cart.getItems()) {
					item.setArticle(articleService.findById(item.getArticleId(), articlesList));
					if (item.getArticle() != null) {
						cart.setTotal(cart.getTotal()
								+ (discountService.calculePriceWithDiscount(item.getArticle(), discounts)
										* item.getQuantity()));
					}
				}
			}
			cart.setTotal(
					cart.getTotal() + deliveryFeesService.calculePriceDelivery(cart.getTotal(), deliveryFeesList));
		}
	}

	public void setArticleService(ArticleService articleService) {
		this.articleService = articleService;
	}

	public void setDeliveryFeesService(DeliveryFeesService deliveryFeesService) {
		this.deliveryFeesService = deliveryFeesService;
	}
	
	public void setDiscountService(DiscountService discountService){
		this.discountService = discountService;
	}

}
