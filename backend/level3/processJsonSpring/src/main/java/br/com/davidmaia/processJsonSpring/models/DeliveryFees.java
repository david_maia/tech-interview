package br.com.davidmaia.processJsonSpring.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

/**
 * 
 * @author davidkmaia
 *
 */
@ApiModel(value="DeliveryFees")
public class DeliveryFees {

	private Long price;
	private ElegibleTransactionVolume elegibleTransactionVolume;

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	@JsonProperty("eligible_transaction_volume")
	public ElegibleTransactionVolume getElegibleTransactionVolume() {
		return elegibleTransactionVolume;
	}

	public void setElegibleTransactionVolume(ElegibleTransactionVolume elegibleTransactionVolume) {
		this.elegibleTransactionVolume = elegibleTransactionVolume;
	}

	public DeliveryFees() {

	}

	public DeliveryFees(Long price, ElegibleTransactionVolume elegibleTransactionVolume) {
		super();
		this.price = price;
		this.elegibleTransactionVolume = elegibleTransactionVolume;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((elegibleTransactionVolume == null) ? 0 : elegibleTransactionVolume.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryFees other = (DeliveryFees) obj;
		if (elegibleTransactionVolume == null) {
			if (other.elegibleTransactionVolume != null)
				return false;
		} else if (!elegibleTransactionVolume.equals(other.elegibleTransactionVolume))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DeliveryFees [price=" + price + ", elegibleTransactionVolume=" + elegibleTransactionVolume + "]";
	}

}
