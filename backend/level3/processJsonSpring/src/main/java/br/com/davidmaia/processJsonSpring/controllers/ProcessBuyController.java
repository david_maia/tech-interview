package br.com.davidmaia.processJsonSpring.controllers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ArrayNode;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Cart;
import br.com.davidmaia.processJsonSpring.models.DeliveryFees;
import br.com.davidmaia.processJsonSpring.models.Discount;
import br.com.davidmaia.processJsonSpring.models.Views;
import br.com.davidmaia.processJsonSpring.services.ProcessBuyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author davidkmaia
 *
 */
@Api(value = "Process buy API", produces = "application/json")
@RestController
public class ProcessBuyController {

	@Autowired
	private ProcessBuyService processBuyService;

	@Autowired
	ObjectMapper mapper;

	/**
	 * Method responsible for process the Json request
	 * 
	 * @param str
	 *            maybe a data.json or a simple json into post method
	 * @return a output.json file
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/processFile", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "Method responsible for process the Json request")
	@ApiResponses({ @ApiResponse(code = 201, message = "Process conclude with sucess") })
	public String processJsonResponseFile(@ApiParam(value = "A String with JSON") @RequestBody String str)
			throws JsonProcessingException, IOException {
		List<Article> articlesList = new ArrayList<Article>();
		List<Cart> carts = new ArrayList<Cart>();
		List<DeliveryFees> deliveryFeesList = new ArrayList<DeliveryFees>();
		List<Discount> discounts = new ArrayList<Discount>();

		readJsonRequest(str, articlesList, carts, deliveryFeesList, discounts);

		carts = processBuyService.processBuyCarts(carts, articlesList, deliveryFeesList, discounts);

		ObjectWriter viewWriter = mapper.writerWithView(Views.Public.class);

		PrintWriter out = new PrintWriter(new FileWriter("output.json", true), true);
		out.write(viewWriter.withRootName("carts").writeValueAsString(carts));
		out.close();
		return viewWriter.withRootName("carts").writeValueAsString(carts);

	}

	/**
	 * Makes read of json
	 * 
	 * @param str
	 * @param articlesList
	 * @param carts
	 * @param deliveryFeesList
	 * @param discounts
	 * @throws IOException
	 * @throws JsonProcessingException
	 */
	private void readJsonRequest(String str, List<Article> articlesList, List<Cart> carts,
			List<DeliveryFees> deliveryFeesList, List<Discount> discounts) throws IOException, JsonProcessingException {
		mapper = new ObjectMapper();
		JsonNode nodeMapper = mapper.readTree(str);

		populeArticles(articlesList, nodeMapper);
		populeCarts(carts, nodeMapper);
		populeDeliveryFees(deliveryFeesList, nodeMapper);
		populeDiscounts(discounts, nodeMapper);
	}

	/**
	 * Reads the articles of json
	 * 
	 * @param articlesList
	 * @param nodeMapper
	 */
	private void populeArticles(List<Article> articlesList, JsonNode nodeMapper) {
		ArrayNode articlesNode = (ArrayNode) nodeMapper.get("articles");
		if (articlesNode != null) {
			Iterator<JsonNode> nodeIt = articlesNode.elements();
			while (nodeIt.hasNext()) {
				JsonNode node = nodeIt.next();
				Article article = mapper.convertValue(node, Article.class);
				articlesList.add(article);
			}
		}
	}

	/**
	 * Reads the carts of json
	 * 
	 * @param carts
	 * @param nodeMapper
	 */
	private void populeCarts(List<Cart> carts, JsonNode nodeMapper) {
		ArrayNode cartsNode = (ArrayNode) nodeMapper.get("carts");
		if (cartsNode != null) {
			Iterator<JsonNode> nodeIt = cartsNode.elements();
			while (nodeIt.hasNext()) {
				JsonNode node = nodeIt.next();
				Cart cart = mapper.convertValue(node, Cart.class);
				carts.add(cart);
			}
		}
	}

	/**
	 * Reads the Delivery fees of json
	 * 
	 * @param deliveryFeesList
	 * @param nodeMapper
	 */
	private void populeDeliveryFees(List<DeliveryFees> deliveryFeesList, JsonNode nodeMapper) {
		ArrayNode deliveryNode = (ArrayNode) nodeMapper.get("delivery_fees");
		if (deliveryNode != null) {
			Iterator<JsonNode> nodeIt = deliveryNode.elements();
			while (nodeIt.hasNext()) {
				JsonNode node = nodeIt.next();
				DeliveryFees delivery = mapper.convertValue(node, DeliveryFees.class);
				deliveryFeesList.add(delivery);
			}
		}
	}

	/**
	 * Reads the dicounts od json
	 * 
	 * @param discounts
	 * @param nodeMapper
	 */
	private void populeDiscounts(List<Discount> discounts, JsonNode nodeMapper) {
		ArrayNode discountNode = (ArrayNode) nodeMapper.get("discounts");
		if (discountNode != null) {
			Iterator<JsonNode> nodeIt = discountNode.elements();
			while (nodeIt.hasNext()) {
				JsonNode node = nodeIt.next();
				Discount dicount = mapper.convertValue(node, Discount.class);
				discounts.add(dicount);
			}
		}

	}

}
