package br.com.davidmaia.processJsonSpring.services;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Discount;

/**
 * 
 * @author davidkmaia
 *
 */

@Service
public class DiscountService {

	/**
	 * Returns price of articles with discount
	 * @param cartValue
	 * @param deliveryFeesList
	 * @return
	 */
	public Long calculePriceWithDiscount(Article article, List<Discount> discounts) {
		Long valueDicounted = article.getPrice();
		if(discounts != null){
			for(Discount discount : discounts){

				if(article.getId() == discount.getArticleId()){
					if(discount.getType().equals("amount")){
						return valueDicounted = valueDicounted - discount.getValue();
					}
					if(discount.getType().equals("percentage")){
						Double discountPrice = ((discount.getValue().doubleValue()/100)*valueDicounted);
						return valueDicounted = (long) (valueDicounted - discountPrice);
					}
				}
				
			}
		}
		return valueDicounted;
	}

}
