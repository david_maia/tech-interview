package br.com.davidmaia.processJsonSpring.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

import io.swagger.annotations.ApiModel;

/**
 * 
 * @author davidkmaia
 *
 */
@ApiModel(value="Cart")
public class Cart extends AbstractModel<Long> {

	private List<Item> items;
	private Long total;

	@JsonView(Views.Internal.class)
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getTotal() {
		return total;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cart other = (Cart) obj;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cart [items=" + items + ", total=" + total + "]";
	}

	public Cart() {
		this.total = 0L;
	}

	public Cart(List<Item> items, Long total) {
		super();
		this.items = items;
		this.total = total;
	}

}
