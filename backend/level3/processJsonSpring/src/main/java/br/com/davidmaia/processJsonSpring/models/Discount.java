package br.com.davidmaia.processJsonSpring.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

/**
 * 
 * @author davidkmaia
 *
 */
@ApiModel(value="Discount")
public class Discount {

	private Long value;
	private Long articleId;
	private String type;

	@JsonProperty("article_id")
	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Discount() {

	}

	public Discount(Long value, Long articleId, String type) {
		super();
		this.value = value;
		this.articleId = articleId;
		this.type = type;
	}

	@Override
	public String toString() {
		return "Discounts [value=" + value + ", articleId=" + articleId + ", type=" + type + "]";
	}

}
