package br.com.davidmaia.processJsonSpring.services;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.davidmaia.processJsonSpring.models.Article;

/**
 * 
 * @author davidkmaia
 *
 */

@Service
public class ArticleService {

	/**
	 * Find By ID of Article
	 * @param articleId
	 * @param articlesList
	 * @return un article
	 */
	public Article findById(Long articleId, List<Article> articlesList) {
		if(articlesList != null){
			for(Article article : articlesList){
				if(articleId == article.getId()){
					return article;
				}
			}
		}
		return null;
	}

}
