package processJsonSpring;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Discount;
import br.com.davidmaia.processJsonSpring.services.DiscountService;

/**
 * 
 * @author davidkmaia
 *
 */
public class DiscountServiceTest {

	private DiscountService discountService = new DiscountService();

	@Before
	public void init() {

	}

	@Test
	public void shouldCalculePriceArticleWithDiscount() {
		Long valueReturned1 = discountService.calculePriceWithDiscount(getArticles().get(0), getDiscounts());
		Assert.assertTrue(75L == valueReturned1);
		
		Long valueReturned2 = discountService.calculePriceWithDiscount(getArticles().get(1), getDiscounts());
		Assert.assertTrue(140L == valueReturned2);
		
		Long valueReturned3 = discountService.calculePriceWithDiscount(getArticles().get(2), getDiscounts());
		Assert.assertTrue(210L == valueReturned3);
		
		Long valueReturned4 = discountService.calculePriceWithDiscount(getArticles().get(3), getDiscounts());
		Assert.assertTrue(300L == valueReturned4);
	}
	
	@Test
	public void shouldCalculePriceArticleWithoutDiscount() {
		Long valueReturned1 = discountService.calculePriceWithDiscount(getArticles().get(4), getDiscounts());
		Assert.assertTrue(500L == valueReturned1);
	}
	
	private List<Article> getArticles() {
		List<Article> list = new ArrayList<Article>();
		for (long i = 1; i <= 5; i++) {
			Article a = new Article();
			a.setId(i);
			a.setName("Article " + i);
			a.setPrice(100 * i);
			list.add(a);
		}
		return list;
	}
	
	private List<Discount> getDiscounts(){
		List<Discount> list = new ArrayList<Discount>();
		Discount d1 = new Discount(25L, 1L, "amount");
		Discount d2 = new Discount(30L, 2L, "percentage");
		Discount d3 = new Discount(30L, 3L, "percentage");
		Discount d4 = new Discount(25L, 4L, "percentage");
		list.add(d1);
		list.add(d2);
		list.add(d3);
		list.add(d4);
		return list;
	}
}
