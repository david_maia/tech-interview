package processJsonSpring;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.davidmaia.processJsonSpring.models.DeliveryFees;
import br.com.davidmaia.processJsonSpring.models.ElegibleTransactionVolume;
import br.com.davidmaia.processJsonSpring.services.DeliveryFeesService;

/**
 * 
 * @author davidkmaia
 *
 */
public class DeliveryFeesServiceTest {

	private DeliveryFeesService deliveryFeesService = new DeliveryFeesService();

	@Before
	public void init() {

	}

	@Test
	public void shouldCalculePriceDelivery() {
		Long valueReturned1 = deliveryFeesService.calculePriceDelivery(2000L, getDeliveryFees());
		Assert.assertTrue(0L == valueReturned1);
		
		Long valueReturned2 = deliveryFeesService.calculePriceDelivery(1800L, getDeliveryFees());
		Assert.assertTrue(400L == valueReturned2);
		
		Long valueReturned3 = deliveryFeesService.calculePriceDelivery(780L, getDeliveryFees());
		Assert.assertTrue(800L == valueReturned3);
	}
	
	@Test
	public void shouldCalculePriceNullDelivery() {
		Long valueReturned1 = deliveryFeesService.calculePriceDelivery(null, getDeliveryFees());
		Assert.assertTrue(0L == valueReturned1);
	}
	
	@Test
	public void shouldCalculePriceDeliveryNull() {
		Long valueReturned1 = deliveryFeesService.calculePriceDelivery(700L, null);
		Assert.assertTrue(0L == valueReturned1);
	}
	
	private List<DeliveryFees> getDeliveryFees(){
		List<DeliveryFees> list = new ArrayList<DeliveryFees>();
		DeliveryFees d1 = new DeliveryFees(800L, new ElegibleTransactionVolume(0L, 1000L));
		DeliveryFees d2 = new DeliveryFees(400L, new ElegibleTransactionVolume(1000L, 2000L));
		DeliveryFees d3 = new DeliveryFees(0L, new ElegibleTransactionVolume(2000L, null));
		list.add(d1);
		list.add(d2);
		list.add(d3);
		return list;
	}
}
