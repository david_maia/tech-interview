package processJsonSpring;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Cart;
import br.com.davidmaia.processJsonSpring.models.Item;
import br.com.davidmaia.processJsonSpring.services.ArticleService;
/**
 * 
 * @author davidkmaia
 *
 */
public class ArticleServiceTest {

	private ArticleService articleService = new ArticleService();

	@Before
	public void init() {

	}

	@Test
	public void shouldReturnUnSpecificArticle() {
		List<Article> list = getArticles();
		Article articleReturned = articleService.findById(2l, list);
		Assert.assertEquals(list.get(1), articleReturned);
	}
	
	@Test
	public void shouldReturnNullArticleNonexistent() {
		List<Article> list = getArticles();
		Article articleReturned = articleService.findById(9l, list);
		Assert.assertNull(articleReturned);
	}
	
	@Test
	public void shouldFindByIdWithNullContainer() {
		List<Article> list = null;
		Article articleReturned = articleService.findById(9l, list);
		Assert.assertNull(articleReturned);
	}
	
	private List<Article> getArticles() {
		List<Article> list = new ArrayList<Article>();
		for (long i = 1; i <= 4; i++) {
			Article a = new Article();
			a.setId(i);
			a.setName("Article " + i);
			a.setPrice(100 * i);
			list.add(a);
		}
		return list;
	}
}
