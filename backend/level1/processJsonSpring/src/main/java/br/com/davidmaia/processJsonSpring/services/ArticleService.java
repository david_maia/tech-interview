package br.com.davidmaia.processJsonSpring.services;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.davidmaia.processJsonSpring.models.Article;

@Service
public class ArticleService {

	public Article findById(Long article_id, List<Article> articlesList) {
		for(Article article : articlesList){
			if(article_id == article.getId()){
				return article;
			}
		}
		return null;
	}

}
