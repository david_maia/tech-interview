package br.com.davidmaia.processJsonSpring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Cart;
import br.com.davidmaia.processJsonSpring.models.Item;

@Service
public class ProcessBuyService {

	@Autowired
	private ArticleService articleService;

	public List<Cart> processBuyCarts(List<Cart> carts, List<Article> articlesList) {
		for (Cart cart : carts) {
			for (Item item : cart.getItems()) {
				item.setArticle(articleService.findById(item.getArticle_id(), articlesList));
			}
		}
		return carts;
	}

	public void setArticleService(ArticleService articleService) {
		this.articleService = articleService;
	}
}
