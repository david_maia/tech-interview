package br.com.davidmaia.processJsonSpring.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ArrayNode;

import br.com.davidmaia.processJsonSpring.models.Article;
import br.com.davidmaia.processJsonSpring.models.Cart;
import br.com.davidmaia.processJsonSpring.models.Views;
import br.com.davidmaia.processJsonSpring.services.ProcessBuyService;

@RestController
public class ProcessBuyController {
	
	@Autowired
	private ProcessBuyService processBuyService;
	
	@Autowired
    ObjectMapper mapper;

	@RequestMapping(value = "/process", method = RequestMethod.POST)
	public String processJson(@RequestBody String str) throws JsonProcessingException, IOException {
		
		List<Article> articlesList = new ArrayList<Article>();
		List<Cart> carts = new ArrayList<Cart>();
		readJsonRequest(str, articlesList, carts);
		
		carts = processBuyService.processBuyCarts(carts, articlesList);
		
		ObjectWriter viewWriter = mapper.writerWithView(Views.Public.class);
		return viewWriter.withRootName("carts").writeValueAsString(carts);
	}

	private void readJsonRequest(String str, List<Article> articlesList, List<Cart> carts) throws IOException, JsonProcessingException {
		mapper = new ObjectMapper();
		JsonNode nodeMapper = mapper.readTree(str);
		
		populeArticles(articlesList, nodeMapper);
		populeCarts(carts, nodeMapper);
	}

	private void populeArticles(List<Article> articlesList, JsonNode nodeMapper) {
		ArrayNode articlesNode = (ArrayNode) nodeMapper.get("articles");
		Iterator<JsonNode> nodeIt = articlesNode.elements();
		while(nodeIt.hasNext()){
			JsonNode node = nodeIt.next();
			Article article = mapper.convertValue(node, Article.class);
			articlesList.add(article);
		}
	}
	
	private void populeCarts(List<Cart> carts, JsonNode nodeMapper) {
		ArrayNode articlesNode = (ArrayNode) nodeMapper.get("carts");
		Iterator<JsonNode> nodeIt = articlesNode.elements();
		while(nodeIt.hasNext()){
			JsonNode node = nodeIt.next();
			Cart cart = mapper.convertValue(node, Cart.class);
			carts.add(cart);
		}
	}
	
	
}
