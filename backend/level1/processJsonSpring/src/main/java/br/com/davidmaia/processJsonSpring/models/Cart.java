package br.com.davidmaia.processJsonSpring.models;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

public class Cart extends AbstractModel<Long> {

	private List<Item> items;

	@JsonView(Views.Internal.class)
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Long getTotal() {
		Long total = 0L;
		for (Item item : this.items) {
			total += item.getArticle().getPrice()*item.getQuantity();
		}
		return total;
	}

}
