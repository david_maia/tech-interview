package br.com.davidmaia.processJsonSpring.models;

public class Article extends AbstractModel<Long> {

	private String name;
	private Long price;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

}
